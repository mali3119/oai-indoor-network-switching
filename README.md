* Change MCC + MNC here: 
`emacs /var/tmp/oai-cn5g-fed/docker-compose/docker-compose-mini-nrf.yaml`

* Deploy core network:
cd /var/tmp/oai-cn5g-fed/docker-compose
sudo python3 ./core-network.py --type start-mini --fqdn no --scenario 1

* Start nodeb: 
sudo /var/tmp/oairan/cmake_targets/ran_build/build/nr-softmodem -E   -O /var/tmp/etc/oai/gnb.sa.band78.fr1.106PRB.usrpx310.conf --sa
